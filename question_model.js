const sqlDb = require("./db.js");

// constructor
const Question = function(question) {
  this.title = question.title;
  this.description = question.description;
  this.answered = question.answered;
};

Question.getAll = (title, result) => {
  let query = "SELECT * FROM question";
  if (title) {
    query += ` WHERE title LIKE '%${title}%'`;
  }
  sqlDb.query(query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("question: ", res);
    result(null, res);
  });
};

Question.create = (newQuestion, result) => {
  sqlDb.query("INSERT INTO question SET ?", newQuestion, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    console.log("created question: ", { id: res.insertId, ...newQuestion });
    result(null, { id: res.insertId, ...newQuestion });
  });
};
Question.findById = (id, result) => {
  sqlDb.query(`SELECT * FROM question WHERE id = ${id}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      console.log("found question: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Question with the id
    result({ kind: "not_found" }, null);
  });
};

Question.updateById = (id, question, result) => {
  sqlDb.query(
    "UPDATE question SET title = ?, description = ?, published = ? WHERE id = ?",
    [question.title, question.description, question.published, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found Question with the id
        result({ kind: "not_found" }, null);
        return;
      }
      console.log("updated question: ", { id: id, ...question });
      result(null, { id: id, ...question });
    }
  );
};
Question.remove = (id, result) => {
  sqlDb.query("DELETE FROM question WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    if (res.affectedRows == 0) {
      // not found Question with the id
      result({ kind: "not_found" }, null);
      return;
    }
    console.log("deleted question with id: ", id);
    result(null, res);
  });
};
Question.removeAll = result => {
  sqlDb.query("DELETE FROM question", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`deleted ${res.affectedRows} questions`);
    result(null, res);
  });
};
module.exports = Question;