const express = require("express");
const app = express();
const question_controller = require("./question_controller.js");

// set port, listen for requests
const PORT = process.env.PORT || 3000;


app.get('/', (req, res) => {

  question_controller.findAll(req, res);

})

app.get('/about', (req, res) => {
  res.send('about')
})


app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});


