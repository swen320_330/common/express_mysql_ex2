CREATE TABLE testdb.question (
	id INT auto_increment NOT NULL,
	title varchar(100) NULL,
	content TEXT NULL,
	is_answered varchar(100) NULL,
	add_datetime TIMESTAMP NULL,
	CONSTRAINT question_PK PRIMARY KEY (id)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;


INSERT INTO testdb.question (title,content,is_answered,add_datetime) VALUES
	 ('Question 1 ','What is your name?',0,'2022-03-11 21:17:56'),
	 ('Q2','Where are you from?',0,'2022-03-11 21:17:56'),
	 ('Ask Why','Why do you do this?',0,'2022-03-11 21:19:40');
