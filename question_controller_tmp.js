const Question = require("../models/question_model.js");
// Create and Save a new Question
exports.create = (req, res) => {
  
};
// Retrieve all Questions from the database (with condition).
exports.findAll = (req, res) => {
  
};
// Find a single Question with a id
exports.findOne = (req, res) => {
  
};
// find all published Questions
exports.findAllPublished = (req, res) => {
  
};
// Update a Question identified by the id in the request
exports.update = (req, res) => {
  
};
// Delete a Question with the specified id in the request
exports.delete = (req, res) => {
  
};
// Delete all Questions from the database.
exports.deleteAll = (req, res) => {
  
};